﻿using Microlite.Extensions.MsCommandLineUtils.Attributes;
using Microlite.Extensions.MsCommandLineUtils.Core;
using Microlite.Extensions.MsCommandLineUtils.Core.Adapter;

namespace Microlite.Extensions.MsCommandLineUtils.Internal
{
    internal static class AppServices
    {
        public static ISetup GetSetup()
        {
            //parsers
            IParser<ArgMap, ArgAttribute> argumentsParser = new ArgumentsParser();
            IParser<OptMap, OptAttribute> optionsParser = new OptionsParser();

            //adapter
            ISettingsAdapter adapter = new SettingsAdapter(argumentsParser, optionsParser);

            //SETUP
            ISetup commandSetup = new CommandSetup(adapter);

            return commandSetup;
        }
    }
}
