﻿using System.Collections.Generic;
using Microlite.Extensions.MsCommandLineUtils.Attributes;

namespace Microlite.Extensions.MsCommandLineUtils.Core.Adapter
{
    internal class SettingsAdapter : ISettingsAdapter
    {
        #region init

        private readonly IParser<ArgMap, ArgAttribute> _argumentsParser;
        private readonly IParser<OptMap, OptAttribute> _optionsParser;

        public SettingsAdapter(IParser<ArgMap,ArgAttribute> argumentsParser, IParser<OptMap, OptAttribute> optionsParser)
        {
            _argumentsParser = argumentsParser;
            _optionsParser = optionsParser;
        }

        #endregion

        public List<ArgMap> TransformArgs<TSettings>() where TSettings : ICommandSettings
        {
            return _argumentsParser.Parse<TSettings>();
        }

        public List<OptMap> TransformOpts<TSettings>() where TSettings : ICommandSettings
        {
            return _optionsParser.Parse<TSettings>();
        }

    }

}
