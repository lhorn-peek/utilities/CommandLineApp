using System;
using System.ComponentModel;
using Microsoft.Extensions.CommandLineUtils;

namespace Microlite.Extensions.MsCommandLineUtils.Core.Adapter
{
    internal sealed class OptMap : Map
    {
        public CommandOption MsOption;

        public override void MapInputToSetting(ref ICommandSettings settings)
        {
            MsOption.ShowInHelpText = true;

            var hasValue = MsOption.HasValue();
            if (!hasValue) return;

            if (SettingMember.PropertyType == typeof(bool))
            {
                SettingMember.SetValue(settings, MsOption.HasValue());
                return;
            }

            var converter = TypeDescriptor.GetConverter(SettingMember.PropertyType);
            var value = converter.ConvertFromInvariantString(MsOption.Value());

            SettingMember.SetValue(settings, value);
        }
    }
}