﻿using System;

namespace Microlite.Extensions.MsCommandLineUtils.Core
{
    public interface ICommand//<in TSettings> where TSettings : ICommandSettings
    {
        string Name { get; }

        string Desc { get; }

        int Run(object settings);

        Type SettingsType { get; }
    }
}
