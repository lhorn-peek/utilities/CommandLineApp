﻿using System;

namespace Microlite.Extensions.MsCommandLineUtils.Attributes
{
    public sealed class ArgAttribute : Attribute
    {
        public string Name { get; }

        public string Desc { get; }

        public ArgAttribute(string name, string desc)
        {
            Name = name;
            Desc = desc;
        }
    }
}
