﻿using System;
using Microsoft.Extensions.CommandLineUtils;

namespace Microlite.Extensions.MsCommandLineUtils.Attributes
{
    public enum OptType { SingleVal = 0, NoVal = 1 }

    public sealed class OptAttribute : Attribute
    {
        public string Template { get; }

        public string Desc { get; }

        public OptType OptType { get; }

        public OptAttribute(string template, string desc, OptType type)
        {
            Desc = desc;
            Template = template;
            OptType = type;
        }

        public OptAttribute(string template, string desc)
        {
            Desc = desc;
            Template = template;
            OptType = OptType.NoVal;
        }

        internal CommandOptionType From => 
            OptType == OptType.NoVal
            ? CommandOptionType.NoValue 
            : CommandOptionType.SingleValue;
    }

    
}
