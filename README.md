# README #

Wrapper around 'Microsoft.Extensions.CommandLineUtils' library for creating CLI apps in .net core and 

NOTE: You will need .net core 1.1 installed on your machine as this project makes use of the re introduced .csproj file

### How to? ###

* Create a new .net core console app
"File\New\Project"...

* Add project reference to CommandLineApp\src\Microlite.Extensions.MsCommandLineUtils.csproj
note: no nuget package up yet!

* add nuget package 'Microsoft.Extensions.CommandLineUtils'

* Edit your project.csproj file and add these 2 nodes to the xml:

```xml
<OutputType>Exe</OutputType>
<RuntimeIdentifiers>win7-x64</RuntimeIdentifiers> 
```
..or any other run time identifiers available.

Your .csproj xml should look similiar to this:

```xml
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <OutputType>Exe</OutputType>
    <AssemblyName>exapp</AssemblyName>
    <TargetFramework>netcoreapp1.1</TargetFramework>
    <RuntimeIdentifiers>win7-x64</RuntimeIdentifiers>
  </PropertyGroup>

  <ItemGroup>
    <ProjectReference Include="..\..\src\Microlite.Extensions.MsCommandLineUtils\Microlite.Extensions.MsCommandLineUtils.csproj" />
  </ItemGroup>

</Project>
```
* Create a new class for a commands parameters:

```csharp
public class OpenBrowserParameters : ICommandSettings
{
    [Arg("url", "url string value")]
    public string Url { get; set; }
}
```

* Create a new command class:

```csharp
public class OpenBrowserCommand : Command<OpenBrowserParameters>
{
    public override string Name => "openbrow"; //name of the command that gets typed in terminal

    public override string Desc => "opens a browser window from cli console";

    public override int Run(OpenBrowserParameters settings)
    {
        var url = settings.Url;

        if (string.IsNullOrEmpty(url)) 
        {
            Console.WriteLine("no url argument supplied.\nOpening google.co.uk instead...");
            url = "https://www.google.co.uk/";
        }

        Process.Start(url);

        return 0;
    }
}
```

* in Program.cs main(string args[]) method...

* Create instance of CommandApp() 

```csharp
class Program
{
    static void Main(string[] args)
    { 
        var app = new CommandApp();
    }
}
```

* Register Command and Parameters. Call .Execute() on command app instance:

```csharp
static void Main(string[] args)
{
    var app = new CommandApp();

    app.Register<OpenBrowserCommand, OpenBrowserParameters>();

    app.Execute(args);
}
```



### Run ###

In visual studio, right-click your .net core console app project, select 'publish'.

Open a terminal in your project \bin\Release\PublishOutput folder and type, eg:

> exapp openbrow "https://www.google.co.uk/"