using Microlite.Extensions.MsCommandLineUtils.Attributes;
using Microlite.Extensions.MsCommandLineUtils.Core;

namespace ExampleApp.Commands.OpenBrowser
{
    public class OpenBrowserParameters : ICommandSettings
    {
        [Arg("url", "url string value")]
        public string Url { get; set; }

    }
}